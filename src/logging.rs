use nickel::{Action, Continue, MiddlewareResult, NickelError, Request, Response};


pub fn request_logger<'mw>(
	request: &mut Request, response: Response<'mw>
) -> MiddlewareResult<'mw> {
	info!("[{}] Received request.", request.path_without_query().unwrap_or("none"));
	Ok(Continue(response))
}

pub fn error_logger<'a>(error: &mut NickelError, request: &mut Request) -> Action {
	error!(
		"[{}] Error: {}",
		request.path_without_query().unwrap_or("none"),
		error.message,
	);
	Continue(())
}
