use std::vec::Vec;

use lettre::email::{Email, EmailBuilder};
use nickel::status::StatusCode;

use controllers::controller_error::ControllerError;
use models::{InvalidModelError, ValidationError};


/// This is the class that Json received from a client gets decoded into when an action is invoked.
#[derive(Debug,RustcDecodable)]
pub struct PartialMail {
	pub to: Option<Vec<String>>,
	pub cc: Option<Vec<String>>,
	pub from: Option<String>,
	pub subject: Option<String>,
	pub body: Option<String>,
	pub content_type: Option<String>,
}

impl PartialMail {
	/// Creates a valid Email from a raw PartialMail, performing validity checks.
	///
	/// # Failures
	///
	/// This function returns an error if a required value is missing.
	pub fn to_email(self) -> Result<Email, ControllerError> {
		let mut validation_errors: Vec<ValidationError> = vec![];
		if self.to.is_none() || self.to.as_ref().unwrap().is_empty() {
			validation_errors.push(
				ValidationError::new("to".to_owned(), "can't be blank".to_owned())
			);
		}
		if self.from.is_none() {
			validation_errors.push(
				ValidationError::new("from".to_owned(), "can't be blank".to_owned())
			);
		}
		if self.subject.is_none() {
			validation_errors.push(
				ValidationError::new("subject".to_owned(), "can't be blank".to_owned())
			);
		}
		if self.body.is_none() {
			validation_errors.push(
				ValidationError::new("body".to_owned(), "can't be blank".to_owned())
			);
		}

		if !validation_errors.is_empty() {
			return Err(ControllerError::from(InvalidModelError::from(validation_errors)));
		}

		let mut email = EmailBuilder::new()
			.from(&*(*self.from.as_ref().unwrap()))
			.subject(&*(*self.subject.as_ref().unwrap()))
			.body(&*(*self.body.as_ref().unwrap()))
			.header((
				"Content-Type",
				&*self.content_type.unwrap_or("text/plain; charset=utf-8".to_owned())
			));

		for string_address in self.to.as_ref().unwrap() {
			email = email.to(&*(*string_address));
		}
		if let Some(cc_list) = self.cc.as_ref() {
			for string_address in cc_list {
				email = email.cc(&*(*string_address));
			}
		}

		match email.build() {
			Ok(email) => Ok(email),
			Err(error) => {
				return Err(ControllerError::new(
					format!(
"Couldn't create email from given parameters: {}
	to: {}
	cc: {}
	from: {}
	subject: {}
	body: {}
",
						error,
						self.to.and_then(|addresses| {
							Some(addresses.join("; "))
						}).unwrap_or("<none>".to_owned()),
						self.cc.and_then(|addresses| {
							Some(addresses.join("; "))
						}).unwrap_or("<none>".to_owned()),
						self.from.unwrap_or("<none>".to_owned()),
						self.subject.unwrap_or("<none>".to_owned()),
						self.body.unwrap_or("<none>".to_owned()),
					),
					StatusCode::InternalServerError,
				))
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use lettre::email::{Email, SendableEmail};
	use nickel::status::StatusCode;
	use super::PartialMail;

	#[test]
	fn to_is_required() {
		let input = PartialMail {
			to: None,
			cc: Some(vec!["".to_owned()]),
			from: Some("".to_owned()),
			subject: Some("".to_owned()),
			body: Some("".to_owned()),
			content_type: Some("".to_owned()),
		};

		match input.to_email() {
			Ok(_) => { panic!("A PartialMail with no \"to\" field should be invalid") },
			Err(error) => {
				assert_eq!(StatusCode::UnprocessableEntity, error.status_code());
			}
		};
	}

	#[test]
	fn cc_is_optional() {
		let input = PartialMail {
			to: Some(vec!["".to_owned()]),
			cc: None,
			from: Some("".to_owned()),
			subject: Some("".to_owned()),
			body: Some("".to_owned()),
			content_type: Some("".to_owned()),
		};

		let result = input.to_email();
		assert!(result.is_ok());
	}

	#[test]
	fn from_is_required() {
		let input = PartialMail {
			to: Some(vec!["".to_owned()]),
			cc: Some(vec!["".to_owned()]),
			from: None,
			subject: Some("".to_owned()),
			body: Some("".to_owned()),
			content_type: Some("".to_owned()),
		};

		match input.to_email() {
			Ok(_) => { panic!("A PartialMail with no \"from\" field should be invalid") },
			Err(error) => {
				assert_eq!(StatusCode::UnprocessableEntity, error.status_code());
			}
		};
	}

	#[test]
	fn subject_is_required() {
		let input = PartialMail {
			to: Some(vec!["".to_owned()]),
			cc: Some(vec!["".to_owned()]),
			from: Some("".to_owned()),
			subject: None,
			body: Some("".to_owned()),
			content_type: Some("".to_owned()),
		};


		match input.to_email() {
			Ok(_) => { panic!("A PartialMail with no \"subject\" field should be invalid") },
			Err(error) => {
				assert_eq!(StatusCode::UnprocessableEntity, error.status_code());
			}
		};
	}

	#[test]
	fn body_is_required() {
		let input = PartialMail {
			to: Some(vec!["".to_owned()]),
			cc: Some(vec!["".to_owned()]),
			from: Some("".to_owned()),
			subject: Some("".to_owned()),
			body: None,
			content_type: Some("".to_owned()),
		};


		match input.to_email() {
			Ok(_) => { panic!("A PartialMail with no \"body\" field should be invalid") },
			Err(error) => {
				assert_eq!(StatusCode::UnprocessableEntity, error.status_code());
			}
		};
	}

	#[test]
	fn content_type_defaults_to_plaintext() {
		let input = PartialMail {
			to: Some(vec!["".to_owned()]),
			cc: Some(vec!["".to_owned()]),
			from: Some("".to_owned()),
			subject: Some("".to_owned()),
			body: Some("".to_owned()),
			content_type: None,
		};

		match input.to_email() {
			Ok(email) => {
				assert!(email.message().contains("Content-Type: text/plain; charset=utf-8"));
			},
			Err(error) => {
				panic!("Expected .to_email() to succeed, but it failed: {}", error);
			}
		}
	}

	#[test]
	fn from_partial_copies_all_properties() {
		let raw_mail_properties = PartialMail {
			to: Some(vec![
				"employee1@example.com".to_owned(),
				"employee2@example.com".to_owned(),
				"employee3@example.com".to_owned(),
			]),
			cc: Some(vec![
				"manager1@example.com".to_owned(),
				"manager2@example.com".to_owned()
			]),
			from: Some("marketing@example.com".to_owned()),
			subject: Some("New Meeting".to_owned()),
			body: Some(
				"We should schedule a meeting to discuss the new marketing data.".to_owned()
			),
			content_type: Some("text/html; charset=utf-8".to_owned()),
		};

		let result = raw_mail_properties.to_email();
		match result {
			Ok(email) => {
				let to_addresses = email.to_addresses();
				assert!(to_addresses.contains(&"employee1@example.com".to_owned()));
				assert!(to_addresses.contains(&"employee2@example.com".to_owned()));
				assert!(to_addresses.contains(&"employee3@example.com".to_owned()));
				assert!(to_addresses.contains(&"manager1@example.com".to_owned()));
				assert!(to_addresses.contains(&"manager2@example.com".to_owned()));
				assert_eq!("marketing@example.com", &*email.from_address());
				assert!(email.message().contains("Subject: New Meeting"));
				assert!(email.message().contains("We should schedule a meeting to discuss the new marketing data."));
				assert!(email.message().contains("Content-Type: text/html; charset=utf-8"));
			},
			Err(error) => {
				panic!("Failed to parse mail properties: {}", error);
			}
		};
	}
}
