use std::error::Error;
use std::convert::From;
use std::fmt::{self, Display, Formatter};

pub mod mail;


#[derive(Debug)]
pub struct InvalidModelError {
	pub errors: Vec<ValidationError>,
}

impl From<Vec<ValidationError>> for InvalidModelError {
	fn from(validation_errors: Vec<ValidationError>) -> Self {
		InvalidModelError {
			errors: validation_errors
		}
	}
}

impl Display for InvalidModelError {
	fn fmt(&self, formatter: &mut Formatter) -> Result<(), fmt::Error> {
		let message = self
			.errors
			.iter()
			.map(|error| error.to_string() )
			.collect::<Vec<String>>()
			.join("; ");
		write!(formatter, "{}", message)
	}
}


/// Represents an invalid value for a model property.
#[derive(Debug)]
pub struct ValidationError {
	field_name: String,
	failure_message: String,
}

impl ValidationError {
	pub fn new(field_name: String, failure_message: String) -> Self {
		ValidationError {
			field_name: field_name,
			failure_message: failure_message,
		}
	}

	pub fn field_name(&self) -> &str {
		&*self.field_name
	}
}

impl Error for ValidationError {
	fn description(&self) -> &str {
		&*self.failure_message
	}
}

impl Display for ValidationError {
	fn fmt(&self, formatter: &mut Formatter) -> Result<(), fmt::Error> {
		write!(formatter, "invalid {}: {}", self.field_name, self.failure_message)
	}
}
