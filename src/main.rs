extern crate docopt;
extern crate env_logger;
extern crate lettre;
#[macro_use] extern crate log;
extern crate logger;
extern crate plugin;
extern crate nickel;
extern crate rustc_serialize;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate typemap;

mod api;
mod controllers;
mod logging;
mod models;
mod smtp_server_settings;

use docopt::Docopt;
use nickel::{Action, Nickel, NickelError, Request};
use smtp_server_settings::SmtpServerMiddleware;


const USAGE: &'static str = "
town-crier - a listening service that converts AJAX requests to emails.

Usage:
  town-crier (-h | --help)
  town-crier <listening-port>

Options:
  -h --help		   Show help text.
";

#[derive(Debug, Deserialize)]
struct Args {
	arg_listening_port: u16,
}

fn main() {
	env_logger::init().expect("Failed to initialize logging backend.");

	let args: Args = Docopt::new(USAGE)
		.and_then(|d| d.deserialize())
		.unwrap_or_else(|e| e.exit());

	let mut app = Nickel::new();

	app.utilize(logging::request_logger);
	app.utilize(SmtpServerMiddleware::new());
	let error_logger: fn(&mut NickelError, &mut Request) -> Action = logging::error_logger;
	app.handle_error(error_logger);
	app.utilize(self::api::town_crier_api());

	let host_string: String = format!("localhost:{}", args.arg_listening_port);
	app.listen(&*host_string).expect(
		&format!("Listening failed using host string {}", host_string)
	);
}
