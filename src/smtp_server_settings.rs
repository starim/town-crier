use std::convert::Into;
use std::env;

use nickel::{Request, Response, Middleware, Continue, MiddlewareResult};
use lettre::transport::smtp::authentication::Mechanism;
use lettre::transport::smtp::{SecurityLevel, SmtpTransport, SmtpTransportBuilder};
use plugin::Extensible;
use typemap::Key;


// Unfortunately Lettre's SmtpTransport and SmtpTransportBuilder aren't Copy types, so it's
// necessary to carry around all the properties required to create an SmtpTransportBuilder and then
// instantiate the builder, then use it to instantiate the SmtpTransport every time a mail needs to
// be sent.
#[derive(Debug, Clone)]
pub struct CloneableSmtpTransportBuilder {
	pub socket_addr: String,
	pub username: String,
	pub password: String,
	pub security_level: String, // unfortunately SecurityLevel doesn't implement Clone
}

impl Into<SmtpTransport> for CloneableSmtpTransportBuilder {
	fn into(self) -> SmtpTransport {
		// note that the unwrap should be safe because the socket_addr was checked for validity
		// when the SmtpServerMiddleware was created
		let security_level = match self.security_level.as_ref() {
			"STARTTLS" => { SecurityLevel::AlwaysEncrypt }
			"SSL/TLS" => { SecurityLevel::EncryptedWrapper }
			"none" => { SecurityLevel::Opportunistic }
			_ => {
				// not that this branch shouldn't be possible since the value is checked to be one
				// of the expected options when it's added to the CloneableSmtpTransportBuilder
				panic!(

					"TOWN_CRIER_SMTP_SECURITY_LEVEL environment variable must be one of \
					STARTTLS, SSL/TLS, or none."
				);
			}
		};
		SmtpTransportBuilder::new(&*self.socket_addr)
			.unwrap()
			.credentials(&*self.username, &*self.password)
			.smtp_utf8(true)
			.authentication_mechanism(Mechanism::Plain)
			.security_level(security_level)
			.build()
	}
}

pub struct SmtpServerMiddleware {
	builder: CloneableSmtpTransportBuilder,
}

impl SmtpServerMiddleware {
	pub fn new() -> SmtpServerMiddleware {
        let hostname: String = env::var("TOWN_CRIER_SMTP_HOSTNAME").ok().expect(
			"TOWN_CRIER_SMTP_HOSTNAME environment variable is unset. It should contain the \
			hostname at which the SMTP server can be reached."
		);
        let port: String = env::var("TOWN_CRIER_SMTP_PORT").ok().expect(
			"TOWN_CRIER_SMTP_PORT environment variable is unset. It should contain the port on \
			which the SMTP server is listening."
		);
        let username: String = env::var("TOWN_CRIER_SMTP_USERNAME").ok().expect(
			"TOWN_CRIER_SMTP_USERNAME environment variable is unset. It should contain the user \
			name required to log in to the SMTP server."
		);
        let password: String = env::var("TOWN_CRIER_SMTP_PASSWORD").ok().expect(
			"TOWN_CRIER_SMTP_PASSWORD environment variable is unset. It should contain the \
			password required to log in to the SMTP server."
		);
        let security_level: String = env::var("TOWN_CRIER_SMTP_SECURITY_LEVEL").ok().expect(
			"TOWN_CRIER_SMTP_SECURITY_LEVEL environment variable is unset. It must be one of \
			STARTTLS, SSL/TLS, or none."
		);
		match security_level.as_ref() {
			"STARTTLS" => {}
			"SSL/TLS" => {}
			"none" => {}
			_ => {
				panic!(

					"TOWN_CRIER_SMTP_SECURITY_LEVEL environment variable must be one of \
					STARTTLS, SSL/TLS, or none."
				);
			}
		};

		let socket_addr = format!("{}:{}", hostname, port);
		if let Err(error) = SmtpTransportBuilder::new(&*socket_addr) {
			panic!("Invalid SMTP server settings: {}", error);
		}

		SmtpServerMiddleware {
			builder: CloneableSmtpTransportBuilder {
				socket_addr: socket_addr,
				username: username,
				password: password,
				security_level: security_level,
			}
		}
	}
}

impl Key for SmtpServerMiddleware { type Value = CloneableSmtpTransportBuilder; }

impl<D> Middleware<D> for SmtpServerMiddleware {
    fn invoke<'mw, 'conn>(&self, req: &mut Request<'mw, 'conn, D>, res: Response<'mw, D>) -> MiddlewareResult<'mw, D> {
        req.extensions_mut().insert::<SmtpServerMiddleware>(self.builder.clone());
        Ok(Continue(res))
    }
}

pub trait SmtpServerRequestExtension {
    fn smtp_server(&self) -> SmtpTransport;
}

impl<'a, 'b, D> SmtpServerRequestExtension for Request<'a, 'b, D> {
    fn smtp_server(&self) -> SmtpTransport {
use std::convert::Into;
        self.extensions().get::<SmtpServerMiddleware>().unwrap().clone().into()
    }
}
