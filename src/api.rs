use std;

use nickel::{
	JsonBody, MediaType, MiddlewareResult, Nickel, Request, Response, Router
};
use nickel::router::http_router::HttpRouter;
use nickel::status::StatusCode;
use rustc_serialize::json::Json;

use controllers::controller_error::ControllerError;
use controllers::mail_controller::MailController;
use models::mail::PartialMail;

use smtp_server_settings::SmtpServerRequestExtension;


pub fn town_crier_api() -> Router {
	// all logic relating to validating parameters, setting headers, or anythiing else directly
	// related to the details of HTTP belongs in the router here, while logic that performs the
	// actual action associated with the route goes in the controller
	let mut router = Nickel::router();

	router.post("/mail", create_mail);
	router
}

fn response_wrapper<'mw>(
	request: &Request,
	mut response: Response<'mw>,
	result: Result<Json, ControllerError>,
) -> MiddlewareResult<'mw> {
	match result {
		Ok(body) => {
			// logging of the response is done here rather than in a dedicated logging middleware
			// like it is for errors because other middlewares in the chain don't seem to be able
			// to retrieve the response body
			info!(
				"[{}] Sending response (HTTP {}):\n\t{}",
				request.path_without_query().unwrap_or("unknown path"),
				response.status(),
				body.to_string(),
			);
			response.set(MediaType::Json);
			response.send(body.to_string())
		},
		Err(error) => {
			response.error(error.status_code(), error.to_string())
		}
	}
}

fn create_mail<'mw>(request: &mut Request, response: Response<'mw>) -> MiddlewareResult<'mw> {
	let parse_error: Option<std::io::Error>;
	match request.json_as::<PartialMail>() {
		Ok(mail) => {
			let mut mailer = request.smtp_server();
			let result =
				MailController::create(mail, &mut mailer).map(|_| {
					Json::from_str("\"success\"").unwrap()
				});
			return response_wrapper(&request, response, result);
		},
		Err(error) => {
			parse_error = Some(error);
		}
	}

	if let Some(error) = parse_error {
		let message = format!(
			"Failed to parse mail data: {}", error.to_string()
		);
		return response_wrapper(
			&request,
			response,
			Err(ControllerError::new(
				message, StatusCode::UnprocessableEntity
			))
		);
	} else {
		unreachable!(
			"mail_create: neither returned a valid response nor encountered an expected error."
		);
	}
}
