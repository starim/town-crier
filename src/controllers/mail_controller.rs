use lettre::email::SendableEmail;
use lettre::transport::EmailTransport;
use lettre::transport::smtp::SmtpTransport;

use controllers::controller_error::ControllerError;
use models::mail::PartialMail;

pub struct MailController;

impl MailController {
	pub fn create(mail_properties: PartialMail, mailer: &mut SmtpTransport) -> Result<(), ControllerError> {
		let email = try!(mail_properties.to_email());
		info!("sending mail to {}...", email.to_addresses().join("; "));
		try!(mailer.send(email));
		info!("mail successfully sent");
		Ok(())
	}
}
