use std::fmt::{Display, Formatter, Result};
use std::error::Error;

use lettre::transport::smtp::error::Error as LettreError;
use nickel::status::StatusCode;

use models::InvalidModelError;


#[derive(Debug)]
pub struct ControllerError {
	message: String,
	status_code: StatusCode,
}

#[allow(dead_code)]
impl ControllerError {
	pub fn new(message: String, status_code: StatusCode) -> Self {
		ControllerError {
			message: message,
			status_code: status_code,
		}
	}

	pub fn status_code(&self) -> StatusCode {
		self.status_code
	}
}

impl Error for ControllerError {
	fn description(&self) -> &str {
		&*self.message
	}
}

impl Display for ControllerError {
	fn fmt(&self, formatter: &mut Formatter) -> Result {
		write!(formatter, "HTTP {}: {}", self.status_code, self.message)
	}
}

impl From<InvalidModelError> for ControllerError {
	fn from(validation_errors: InvalidModelError) -> Self {
		ControllerError::new(
			validation_errors.to_string(),
			StatusCode::UnprocessableEntity,
		)
	}
}

impl From<LettreError> for ControllerError {
	fn from(error: LettreError) -> Self {
		let specific_error = match error.cause() {
			Some(cause) => cause.to_string(),
			None => error.to_string()
		};
		ControllerError::new(
			format!("Error communicating with SMTP server: {}", specific_error),
			StatusCode::InternalServerError,
		)
	}
}
