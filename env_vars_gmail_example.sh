#!/usr/bin/env bash

# Copy this file to env_vars.sh and change any values to work for your system.
# Before running Town Crier, execute "source env_vars.sh" from the same shell
# that will run Town Crier.

export RUST_LOG=town_crier=info
export TOWN_CRIER_SMTP_HOSTNAME='smtp.gmail.com'
export TOWN_CRIER_SMTP_PORT='465'
export TOWN_CRIER_SMTP_SECURITY_LEVEL='SSL/TLS'
export TOWN_CRIER_SMTP_USERNAME='your email address'
export TOWN_CRIER_SMTP_PASSWORD='your password'
