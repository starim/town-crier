#!/usr/bin/env bash

# Copy this file to env_vars.sh and change any values to work for your system.
# Before running Town Crier, execute "source env_vars.sh" from the same shell
# that will run Town Crier.

export RUST_LOG=town_crier=info
export TOWN_CRIER_SMTP_HOSTNAME='the hostname at which the SMTP server can be reached'
export TOWN_CRIER_SMTP_PORT='the port on which the SMTP server is listening'
export TOWN_CRIER_SMTP_SECURITY_LEVEL='must be one of STARTTLS, SSL/TLS, or none'
export TOWN_CRIER_SMTP_USERNAME='the user name to log in to the SMTP server'
export TOWN_CRIER_SMTP_PASSWORD='the password to log in to the SMTP server'
