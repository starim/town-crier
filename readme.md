Town Crier
==

Town Crier is a lightweight program that implements a REST API for sending
emails. The best use case for this service is to allow a webapp to send emails
via AJAX requests to Town Crier's API.

Note: This system provides no security. Using it means that anyone who can send HTTP POST traffic to your domain can send mail from your mail server.

Installation
--

1. [Install Rust if needed.](https://doc.rust-lang.org/book/installing-rust.html) Town Crier works on stable, beta, and nightly Rust.
2. Make sure the system you will be running Town Crier from has OpenSSL's runtime libraries and headers installed, see [the rust-openssl readme](https://github.com/sfackler/rust-openssl) for instructions.
2. Install tmux or screen if you want Town Crier to run as a persistent service.
3. Clone [the Town Crier repository](https://github.com/starim/town-crier)
4. Move into the cloned repostiory and compile with `cargo build --release`
5. Copy the executable at `<project root>/target/release/town-crier` to the server or filesystem location of your choice.
6. Set environment variables for specifying SMTP server connection info that Town Crier will use. `env_vars_example.sh` provides a general example, and `env_vars_gmail_example.sh` provides settings for communicating with Gmail specifically. Copy the desired example script, edit your copy to contain the right values for your SMTP server, and run `source <your environment variable script>`. If you have a dedicated user account on a server for running Town Crier as a service, add `source <your environment variable script>` to that user's `.bashrc` so that it's loaded automatically for that user.
7. If you want Town Crier to run as a persistent service, start tmux or screen.
7. Run Town Crier with `./town-crier <listening port> > <log file> 2>&1` e.g. `./town-crier 5000 > ~/town-crier.log 2>&1` to start Town Crier listening on port 5000 and logging to `town-crier.log` in the user's home folder.
8. Add an Apache virtual host or nginx server entry for Town Crier to serve the API to external HTTP clients.


API
--

`POST /mail`: sends an email with the properties given:

```
{
	to (array of strings): email addresses of recipients,
	cc (array of strings; optional): email addresses of people who should be CC'd,
	from (string): the email address the mail will appear to originate from*,
	subject (string): the subject line for the email,
	body (string): the body content of the email,
	content_type (string; optional): the Content-Type header value (e.g. "text/html; charset=utf-8" or "text/plain; charset=utf-8"), defaults to "text/plain; charset=utf-8" (note that this API expects all text to be UTF-8 and all emails it sends are UTF-8 encoded)
}
```

`*` note that most SMTP servers will override this value with the address of the account you log into the SMTP server with

example:

```
{
	to: ["employee1@example.com", "employee2@example.com"],
	cc: ["manager@example.com"],
	from: "marketing@example.com",
	subject: "New Meeting",
	body: "We should schedule a meeting to discuss the new marketing data.",
}
```
response:
"success" (status code: 200 OK)

An email of the following form will be sent:
```
From: <marketing@example.com>
Subject: New Meeting
Content-Type: text/plain; charset=utf-8
To: <employee1@example.com>
To: <employee2@example.com>
Cc: <manager@example.com>
Date: Tue, 05 Jan 2016 15:14:18 -0800
Message-ID: <c0480a7f-21e2-4dd8-a625-352e1f9eed6c.lettre@localhost>

We should schedule a meeting to discuss the new marketing data.
```

Error Responses
--

If you send a request with missing properties, or if the request can't be parsed as JSON, you will receive an HTTP 422 Unprocessable Entity status code in response.

example:
```
{
	subject: "New Meeting",
	from: "marketing@example.com",
	body: "We should schedule a meeting to discuss the new marketing data."
}
```
response:
"to can't be blank" (status code: 422 Unprocessable Entity)


Sending a parameter of the wrong data type will also result in a 422: Unprocessable Entity response. In this case, the to value should be an array with a single address, but instead a simple string is sent:
```
{
	to: "employee@example.com"
	subject: "New Meeting",
	from: "marketing@example.com",
	body: "We should schedule a meeting to discuss the new marketing data."
}
```
response:
"Failed to parse mail data: Parse error: ExpectedError("Array", "\"employee@example.com\"")" (status code: 422 Unprocessable Entity)
